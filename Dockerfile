# gets python image
FROM python:3.9

#setting up a working directory
WORKDIR /code

# copy the requirements file to this directory
COPY ./requirements.txt /code/requirements.txt

# install the dependencies 
RUN pip install --no-cache-dir --upgrade -r /code/requirements.txt

# copy the app 
COPY ./app /code/app

# run the commands
CMD ["uvicorn", "app.main:app", "--host", "0.0.0.0", "--port", "80"]
