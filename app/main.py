from fastapi import FastAPI

import os

app = FastAPI()


@app.get("/")
async def root():
    return {"Hello": f"from: {os.environ.get('HOSTNAME', 'DEFAULT_ENV')}"}
